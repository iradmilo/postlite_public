const passport = require('passport');


module.exports = (app) => {
    //route Handler for authentification with Google
    app.get('/auth/google', passport.authenticate('google', {
        scope: ['profile', 'email']
    })
    );

    app.get('/auth/google/callback', passport.authenticate('google'),
        (req, res) => {
            res.redirect('/top-posts');
        }
    );

    app.get('/api/logout',
        (req, res) => {
            req.logout();
            res.redirect('/');
        });

    app.get('/api/current_user',
        (req, res) => {
            res.send(req.user);
        });
};


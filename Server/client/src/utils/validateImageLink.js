const re = /(http)?s?:?(\/\/[^"']*\.(?:png|jpg|jpeg|png|svg))/i;
// const reY = /http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/
// var regexYoutube = new RegExp(reY);
var regex = new RegExp(re);

export default link => {
    return link.match(regex);
};
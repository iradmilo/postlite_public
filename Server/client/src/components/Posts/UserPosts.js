import React from 'react';
import { Link } from 'react-router-dom';
import PostList from './PostList';

const UserPosts = () => {
  return (
    <div style={{ margin: '10px 10px' }}>
      <PostList />
      <div className="fixed-action-btn">
        <Link
          to="/userposts/new"
          className="btn-floating btn-large red lighten-1 waves-effect waves-light red"
        >
          <i className="material-icons">add</i>
        </Link>
      </div>
    </div>
  );
};

export default UserPosts;

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPost, deletePost, upvotePost, downvotePost } from '../../actions';
import { Link } from 'react-router-dom';
import { OverlayTrigger, Tooltip, Image } from 'react-bootstrap';
import validateImageLink from '../../utils/validateImageLink';
import image from '../../images/image.png';


class PostShow extends Component {

  componentDidMount() {
    //if(!this.props.post){
    const { id } = this.props.match.params;
    this.props.fetchPost(id);
  }

  onDeleteClick() {
    const { id } = this.props.match.params;
    this.props.deletePost(id, this.props.history);
  }

  onUpvoteClick() {
    const { id } = this.props.match.params;
    this.props.upvotePost(id);

  }

  onDownvoteClick() {
    const { id } = this.props.match.params;
    this.props.downvotePost(id);

  }

  renderDeleteAndEditButton(post) {
    if (this.props.auth && this.props.auth._id === post._userID) {
      return [
        <Link key="1" className="btn blue lighten-3 edit-btn center-align" to={`/userposts/edit/${post._id}`}>
          <i className="material-icons left">mode_edit</i>
          <span className="hide-on-small-only">Edit post</span>
        </Link>,
        <button key="2"
          className="btn red right-align"
          onClick={this.onDeleteClick.bind(this)}
        >
          <i className="material-icons left">delete</i>
          <span className="hide-on-small-only">Delete post</span>
        </button>
      ];
    }
  }

  render() {
    //this.props === ownProps
    const { post } = this.props;
    if (!post) {
      return <div>Loading...</div>;
    }
    const tooltip = (
      <Tooltip id="tooltip"><strong>{post.link}</strong></Tooltip>
    );

    return (

      <div>
        <div className="card horizontal card-panel hoverable" style={{ 'overflow': 'auto' }}>
          <div className="card-stacked" style={{ 'overflow': 'auto' }}>
            <div className="card-content">

              <div className="card-image right hide-on-small-and-down">
                <Image href="#" src={`${validateImageLink(post.link) ? post.link : image}`} alt="image" style={{ 'maxWidth': '100%', 'maxHeight': '100%' }} />
              </div>
              <div className="left post-desc col s12">
                <span className="card-title left-align">{post.title}</span>
                <p style={{ 'margin': '20px 0px 50px 0px' }}>{post.text}</p>

                <div>
                  Post by: {post.username}
                </div>
                <div>
                  Created On: {new Date(post.dateCreated).toLocaleDateString()}

                </div>
              </div>
            </div>
            <div className="card-action">
              <div className="left">
                <OverlayTrigger placement="top" overlay={tooltip}>
                  <a className="btn orange lighten-2 tooltipped "
                    href={post.link}
                  ><i className="material-icons left">link</i>
                    <span className="hide-on-small-only">Link</span></a>
                </OverlayTrigger>
              </div>
              <div className="right" style={{ marginLeft: '20px' }} >
                <div className="left" >
                  <span className="left voteColor badge teal lighten-2 vote-number">
                    {post.upvotes}
                  </span>
                  <button
                    className={`btn right teal  ${this.props.post !== undefined && this.props.post.userVotes !== undefined &&
                      this.props.post.userVotes === 1 ? 'darken-4' : 'lighten-4'}`}
                    onClick={this.onUpvoteClick.bind(this)}
                  >
                    <i className="material-icons left">thumb_up</i>
                    <span className="hide-on-small-only">Upvote</span>
                  </button>
                </div>
                <div className="right" style={{ 'marginLeft': '20px' }}>
                  <span className="left voteColor badge teal lighten-2 vote-number">
                    {post.downvotes}
                  </span>
                  <button
                    className={`btn right teal ${this.props.post !== undefined && this.props.post.userVotes !== undefined &&
                      this.props.post.userVotes === -1 ? 'darken-4' : 'lighten-4'}`}
                    onClick={this.onDownvoteClick.bind(this)}
                  >
                    <i className="material-icons left">thumb_down</i>
                    <span className="hide-on-small-only">Downvote</span>
                  </button>

                </div>
              </div>
            </div>
            <div className="card-action card-actions valign-wrapper">
              <Link className="btn btn-primary left-align" to="/userposts">
                <i className="material-icons left">arrow_back</i>
                <span className="hide-on-small-only">Back To index</span>
              </Link>
              {this.renderDeleteAndEditButton(post)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ userPosts, auth }, ownProps) {
  return {
    post: userPosts[ownProps.match.params.id],
    auth: auth
  };
}

export default connect(mapStateToProps, { fetchPost, deletePost, upvotePost, downvotePost })(PostShow);

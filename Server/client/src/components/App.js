import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import './App.css';
import * as actions from '../actions';

import Header from './Header';
import UserPosts from './Posts/UserPosts';
import PostNew from './Posts/PostNew';
import TopPosts from './TopPosts';
import PostShow from './Posts/PostShow';
import PostEdit from './Posts/PostEdit';
import Landing from './Landing';

class App extends Component {
  componentDidMount() {
    this.props.fetchUser();
  }

  render() {
    return (
      <div className="container">
        <BrowserRouter>
          <div>
            <Header />
            <Route exact path="/" component={Landing} />
            <Route exact path="/top-posts" component={TopPosts} />
            <Route exact path="/userposts" component={UserPosts} />
            <Route exact path="/userposts/new" component={PostNew} />
            <Route exact path="/userposts/posts/:id" component={PostShow} />
            <Route exact path="/userposts/edit/:id" component={PostEdit} />
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default connect(null, actions)(App);
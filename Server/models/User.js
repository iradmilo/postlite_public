const mongoose = require('mongoose');
const { Schema } = mongoose;


//enum mongoose property for allowing property to only have certain values 
const userSchema = new Schema({
  googleID: String,
  username: String,
  dateCreated: Date
});

//Create modelClass of users
mongoose.model('users', userSchema);
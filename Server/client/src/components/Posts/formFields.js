export default [
  { label: 'Title', name: 'title', length: 50 },
  { label: 'Text', name: 'text', length: 500 },
  { label: 'Link', name: 'link' }
]
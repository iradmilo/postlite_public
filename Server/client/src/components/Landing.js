import React from 'react';

const Landing = () => {
  return (
    <div style={{ textAlign: 'center' }}>
      <h1>
        PostLite!
      </h1>
      <p>  Go to top posts or login to vote and see your posts </p>
    </div>
  );
};

export default Landing;
import _ from 'lodash';
import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { Link } from 'react-router-dom';
import PostField from './PostField';
import formFields from './formFields';

class PostForm extends Component {
  renderFields() {
    return _.map(formFields, ({ label, name }) => {
      return (
        <Field
          key={name}
          component={PostField}
          type="text"
          label={label}
          name={name}
        />
      );
    });
  }

  render() {
    return (
      <div>
        <form onSubmit={this.props.handleSubmit(this.props.onPostSubmit)}>
          {this.renderFields()}
          <Link
            to="/userposts"
            className="red btn-flat white-text btn btn-danger"
          >
            Cancel
          </Link>
          <button type="submit" className="teal btn-flat right white-text">
            Submit Post
            <i className="material-icons right">done</i>
          </button>
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (values.length > 500) {
    errors["title"] = "Title must be less than 500 charachters long";
  }
  _.each(formFields, ({ label, name, length }) => {
    if (values[name] && values[name].length > length) {
      errors[name] = `${label} should be less than ${length} characters long`;
    }
  });

  _.each(formFields, ({ name }) => {
    if (!values[name] && name !== 'link') {
      errors[name] = `You should provide ${name}`;
    }
  });

  return errors;
}

export default reduxForm({
  validate,
  form: 'postForm',
  destroyOnUnmount: false
})(PostForm);

const mongoose = require('mongoose');
const { Schema } = mongoose;

//moongose min doesn't work here check!!!
const postSchema = new Schema({
	title: {
		type: String,
		validate: {
			validator: title => title.length <= 50,
			message: 'Name must not be longer than 50 characters'
		},
		required: [true, 'Title is required']
	},
	text: {
		type: String,
		validate: {
			validator: text => text.length <= 500,
			message: 'Name must not be longer than 500 characters'
		},
		required: [true, 'Text is required']
	},
	link: String,
	dateCreated: Date,
	upvotes: { type: Number, min: 0, default: 0 },
	downvotes: { type: Number, min: 0, default: 0 },
	_userID: { type: Schema.Types.ObjectId, ref: 'User' },
	username: String,
	votedPosts: [
		{
			_id: false,
			userId: { type: Schema.Types.ObjectId },
			votes: { type: Number, enum: [-1, 1] }
		}
	]
});

//Create modelClass of posts
mongoose.model('posts', postSchema);

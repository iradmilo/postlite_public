import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import _ from 'lodash';
import { Link } from 'react-router-dom';


class TopPostList extends Component {
  constructor(props) {
    super(props);
    this.onDownvoteClick = this.onDownvoteClick.bind(this);
    this.onUpvoteClick = this.onUpvoteClick.bind(this);
    this.onDeleteClick = this.onDeleteClick.bind(this);
  }

  onDeleteClick(id) {
    this.props.deletePost(id, null);
  }

  onUpvoteClick(id) {
    this.props.upvotePost(id);
  }

  onDownvoteClick(id) {
    this.props.downvotePost(id);
  }

  componentDidMount() {
    this.props.fetchTopPosts();
  }

  renderDeleteButton(post) {
    if (this.props.auth && this.props.auth._id === post._userID) {
      return (
        <button
          className="btn red btn-floating "
          onClick={() => this.onDeleteClick(post._id)}
        >
          <i className="material-icons left">delete</i>
        </button>
      );
    }
  }

  renderLink(post) {
    if (this.props.auth) {
      return (
        <Link to={`/userposts/posts/${post._id}`}>
          <span className="card-title left-align">{post.title}</span>
        </Link>
      );
    }
    else {
      return (
        <span className="card-title left-align">{post.title}</span>
      );
    }
  }

  renderTopPosts() {
    return _.map(this.props.topPosts, post => {
      return (
        <div key={post._id} className="col s12 m12 l12">
          <div className="card horizontal card-panel hoverable">
            <div className="card-stacked" style={{ 'overflow': 'auto' }}>
              <div className="float right" style={{ textAlign: 'right' }}>
                {this.renderDeleteButton(post)}
              </div>
              <div className="card-stacked">
                <div className="card-content post-desc-list">
                  {this.renderLink(post)}
                  <p style={{ 'margin': '20px 0px 40px 0px' }}>{post.text}</p>
                  <div>
                    <div className="left">
                      Post by: {post.username}
                    </div>
                    <div className="right">
                      Created On: {new Date(post.dateCreated).toLocaleDateString()}
                    </div>
                  </div>
                </div>
                <div className="card-action">
                  <a className="card-link-list" href={post.link}>{post.link}</a>
                  <div className="right" style={{ 'margin': '20px 0' }}>
                    <div className="left">
                      <span className="left voteColor badge teal lighten-2 vote-number">
                        {post.upvotes}
                      </span>
                      <button
                        className={`btn left teal ${post !== undefined && post.userVotes !== undefined &&
                          post.userVotes === 1 ? 'darken-4' : 'lighten-4'} ${this.props.auth ? 'enabled' : 'disabled'}`}
                        onClick={() => this.onUpvoteClick(post._id)}
                      >
                        <i className="material-icons left">thumb_up</i>
                      </button>
                    </div>
                    <div className="right" style={{ marginLeft: '20px' }}>
                      <span className="left voteColor badge teal lighten-2 vote-number">
                        {post.downvotes}
                      </span>
                      <button
                        className={`btn right teal ${post !== undefined && post.userVotes !== undefined &&
                          post.userVotes === -1 ? 'darken-4' : 'lighten-4'} ${this.props.auth ? 'enabled' : 'disabled'}`}
                        onClick={() => this.onDownvoteClick(post._id)}
                      >
                        <i className="material-icons left">thumb_down</i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    return (
      <div>
        {this.renderTopPosts()}
      </div>
    );
  }
}

function mapStateToProps({ topPosts, auth }) {
  return { topPosts, auth };
}

export default connect(mapStateToProps, actions)(TopPostList);
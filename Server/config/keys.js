if (process.env.NODE_ENV === 'production') {
    //keys in production
    module.exports = require('./prod');
} else {
    //keys in development
    module.exports = require('./dev');
}
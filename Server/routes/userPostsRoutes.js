const _ = require('lodash');
const Path = require('path-parser');
const { URL } = require('url');
const mongoose = require('mongoose');
const requireLogin = require('../middlewares/requireLogin');

const Post = mongoose.model('posts');
const User = mongoose.model('users');

module.exports = app => {
    app.get('/api/userposts', requireLogin, async (req, res) => {
        try {
            //if a record of current user voting on one of these posts exists in
            // votedPosts subdocument, extract votedPosts.votes and add it as a field to response posts
            const posts = await Post.find({
                _userID: req.user.id,
                'votedPosts.userId': { $ne: req.user.id }
            }).select({ votedPosts: false });
            AggregatePosts(posts, req, res);
        } catch (err) {
            //method not allowed
            res.status(400).send({ error: 'Error while fetching specific post' });
        }
    });

    function AddUserVotesProperty(post, req, res, error) {
        //if a record of current user voting on this post exists in
        // votedPosts subdocument, extract votedPosts.votes and add it as a field to response post
        if (post) {
            let newPost = {};
            newPost = post._doc;;
            let userVotedPosts = _.find(newPost.votedPosts, { 'userId': mongoose.Types.ObjectId(req.user.id) });
            if (userVotedPosts) {
                newPost.userVotes = userVotedPosts.votes;
                ({ votedPosts, ...postSend } = newPost);

                res.status(200).send(postSend);
            } else {
                ({ votedPosts, ...postNew } = newPost);

                res.status(200).send(postNew);
            }
        }
        else throw new Error(error);

    }

    function AggregatePosts(posts, req, res) {
        Post.aggregate(
            {
                $match: {
                    _userID: mongoose.Types.ObjectId(req.user.id),
                    'votedPosts.userId': mongoose.Types.ObjectId(req.user.id)
                }
            },
            {
                $unwind: {
                    path: '$votedPosts',
                    preserveNullAndEmptyArrays: false
                }
            },
            {
                $match: {
                    'votedPosts.userId': mongoose.Types.ObjectId(req.user.id)
                }
            },
            { $addFields: { userVotes: '$votedPosts.votes' } },
            { $project: { votedPosts: false } },
            function (err, result) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    let list = _.union(posts, result);

                    res.status(200).send(list);
                }
            }
        );
    }

    //fetching specific post
    app.get('/api/userposts/posts/:id', requireLogin, async (req, res) => {
        try {
            const post = await Post.findOne()
                .where({ _id: req.params.id });

            AddUserVotesProperty(post, req, res, "Error while fetching specific post");

        } catch (err) {
            //method not allowed
            res.status(400).send({ error: 'Error while fetching specific post' });
        }
    });

    //Geting all posts
    app.get('/api/posts/top', async (req, res) => {
        try {
            if (req.user !== undefined) {
                // votedPosts subdocument, extract votedPosts.votes and add it as a field to response posts
                const posts = await Post.find({
                    'votedPosts.userId': { $ne: req.user.id }
                }).select({ votedPosts: false });
                Post.aggregate(
                    {
                        $match: {
                            'votedPosts.userId': mongoose.Types.ObjectId(req.user.id)
                        }
                    },
                    {
                        $unwind: {
                            path: '$votedPosts',
                            preserveNullAndEmptyArrays: false
                        }
                    },
                    {
                        $match: {
                            'votedPosts.userId': mongoose.Types.ObjectId(req.user.id)
                        }
                    },
                    { $addFields: { userVotes: '$votedPosts.votes' } },
                    { $project: { votedPosts: false } },
                    function (err, result) {
                        if (err) {
                            res.status(400).send(err);
                        } else {
                            let list = _.union(posts, result);
                            list = _.orderBy(list, ['upvotes'], ['desc']);
                            res.status(200).send(list);
                        }
                    }
                );
            } else {
                const posts = await Post.find()
                    .select({ votedPosts: false })
                    .sort({ upvotes: -1 });
                res.status(200).send(posts);
            }
        } catch (err) {
            //method not allowed
            res.status(400).send({ error: 'Error while fetching specific post' });
        }
    });

    //Delete specific post use this on frontend
    app.delete('/api/userposts/:id', requireLogin, async (req, res) => {
        try {
            const post = await Post.findOne({ _id: req.params.id });

            if (post._userID == req.user.id) {
                const deletedPost = await Post.findOneAndRemove({
                    _id: req.params.id
                });
                res.status(200).send(deletedPost._id);
            } else {
                res.status(400).send({
                    error: 'Not allowed to delete this specific post'
                });
            }
        } catch (err) {
            //method not allowed
            res.status(405).send(err);
        }
    });

    //upvoting and downvoting system
    app.patch('/api/userposts/upvote/:id', requireLogin, async (req, res) => {
        //upvote
        try {
            let post = await Post.findOne({ _id: req.params.id });

            let votedPost = await post.votedPosts.filter(function (post) {
                return post.userId == req.user.id;
            })[0];

            //user wants to cancel his upvote

            if (votedPost === undefined) {
                let newPost = await Post.findByIdAndUpdate(
                    {
                        _id: req.params.id
                    },
                    {
                        $inc: { upvotes: 1 },
                        $addToSet: {
                            votedPosts: { userId: req.user.id, votes: 1 }
                        }
                    },
                    { safe: true, upsert: true, new: true }
                );
            } else if (votedPost.votes === 1) {
                newPost = await Post.updateOne(
                    {
                        _id: req.params.id,
                        votedPosts: {
                            $elemMatch: { userId: req.user.id }
                        }
                    },
                    {
                        $inc: { upvotes: -1 },
                        $set: { 'votedPosts.$.votes': 0 }
                    },
                    { new: true }
                );
            } else if (votedPost.votes === 0) {
                newPost = await Post.updateOne(
                    {
                        _id: req.params.id,
                        votedPosts: {
                            $elemMatch: { userId: req.user.id }
                        }
                    },
                    {
                        $inc: { upvotes: 1 },
                        $set: { 'votedPosts.$.votes': 1 }
                    },
                    { new: true }
                );
            } else if (votedPost.votes === -1) {
                newPost = await Post.updateOne(
                    {
                        _id: req.params.id,
                        votedPosts: {
                            $elemMatch: { userId: req.user.id }
                        }
                    },
                    {
                        $inc: { upvotes: 1, downvotes: -1 },
                        $set: { 'votedPosts.$.votes': 1 }
                    },
                    { new: true }
                );
            }
            post = await Post.findOne({ _id: req.params.id });
            AddUserVotesProperty(post, req, res, "Error while upvoting post");

        } catch (err) {
            //method not allowed
            res.status(400).send(err);
        }
    });

    //downvote
    app.patch('/api/userposts/downvote/:id', requireLogin, async (req, res) => {
        try {
            let post = await Post.findOne({ _id: req.params.id });

            let votedPost = await post.votedPosts.filter(function (post) {
                return post.userId == req.user.id;
            })[0];

            if (votedPost === undefined) {
                let newPost = await Post.findByIdAndUpdate(
                    {
                        _id: req.params.id
                    },
                    {
                        $inc: { downvotes: 1 },
                        $addToSet: {
                            votedPosts: { userId: req.user.id, votes: -1 }
                        }
                    },
                    { safe: true, upsert: true, new: true }
                );
            } else if (votedPost !== undefined && votedPost.votes === -1) {
                newPost = await Post.updateOne(
                    {
                        _id: req.params.id,
                        votedPosts: {
                            $elemMatch: { userId: req.user.id }
                        }
                    },
                    {
                        $inc: { downvotes: -1 },
                        $set: { 'votedPosts.$.votes': 0 }
                    },
                    { new: true }
                );
            } else if (votedPost.votes === 1) {
                newPost = await Post.updateOne(
                    {
                        _id: req.params.id,
                        votedPosts: {
                            $elemMatch: { userId: req.user.id }
                        }
                    },
                    {
                        $inc: { upvotes: -1, downvotes: +1 },
                        $set: { 'votedPosts.$.votes': -1 }
                    },
                    { new: true }
                );
            } else if (votedPost.votes === 0) {
                newPost = await Post.updateOne(
                    {
                        _id: req.params.id,
                        votedPosts: {
                            $elemMatch: { userId: req.user.id }
                        }
                    },
                    {
                        $inc: { downvotes: +1 },
                        $set: { 'votedPosts.$.votes': -1 }
                    },
                    { new: true }
                );
            }
            post = await Post.findOne({ _id: req.params.id });
            AddUserVotesProperty(post, req, res, "Error while downvoting post");

        } catch (err) {
            //method not allowed
            res.status(400).send(err);
        }
    });

    //update post route
    app.patch('/api/userposts/edit/:id', requireLogin, async (req, res) => {
        const { title, text, link } = req.body;
        let updatedPost = {};

        try {
            let post = await Post.findOne({ _id: req.params.id });
            if (post._userID == req.user.id && post) {
                //ok

                if (title != null) updatedPost.title = title;
                else throw new Error();

                if (text != null) updatedPost.text = text;
                else throw new Error();

                if (link != null) updatedPost.link = link;
                else throw new Error();

                updatedPost.dateCreated = new Date();
                updatedPost.dateCreated = Date.now();

                newPost = await Post.findByIdAndUpdate(
                    { _id: req.params.id },
                    { $set: updatedPost },
                    { new: true }
                );

                AddUserVotesProperty(newPost, req, res, "Error while updating post");
            } else {
                res.status(422).send({ error: 'Not authorized to see this post' });
            }
        } catch (err) {
            //method not allowed
            res.status(400).send({ error: 'Error during editing posts' });
        }
    });

    //Create new post
    app.post('/api/userposts', requireLogin, async (req, res) => {
        const { title, text, link } = req.body;

        try {
            const post = new Post({
                title,
                text,
                link,
                dateCreated: Date.now(),
                _userID: req.user.id,
                username: req.user.username
            });

            await post.save();
            AddUserVotesProperty(post, req, res, "Error while creating new post");
        } catch (err) {
            res.status(422).send(err);
        }
    });
};

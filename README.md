# README #

### What is this repository for? ###

* PostLite is a responsive web app for sharing posts. It is inspired by Reddit. 
* Version [1.0.0]

###Tech###

* React/Redux
* Express
* MongoDB
* Mongoosejs
* Google OAuth 2.0
* MaterializeCSS
* Bootstrap
* mLab mongo online database

### How do I get set up? ###

* $ cd Server
* $ npm install
* $ cd Server/client
* $ npm install
* in Server run $ npm run dev
* the app should open on localhost:3000

### Who do I talk to? ###

* Bitbucketjrad
* iradmilo
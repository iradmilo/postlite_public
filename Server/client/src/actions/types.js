export const FETCH_USER = 'fetch_user';
export const FETCH_USER_POSTS = 'fetch_user_posts';
export const FETCH_TOP_POSTS = 'fetch_top_posts';
export const FETCH_POST = 'fetch_post';
export const DELETE_POST = 'delete_post';

import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import PostEditForm from './PostEditForm';
import PostEditFormReview from './PostEditFormReview';


class PostEdit extends Component {
    state = { showFormReview: false };


    renderContent() {
        if (this.state.showFormReview) {
            return (
                <PostEditFormReview
                    id={this.props.match.params.id}
                    onCancel={() => this.setState({ showFormReview: false })}
                />
            );
        }

        return (
            <PostEditForm
                id={this.props.match.params.id}
                onPostEdit={() => this.setState({ showFormReview: true })}
            />
        );
    }

    render() {
        return <div>{this.renderContent()}</div>;
    }
}


export default reduxForm({
    form: 'postEditForm'
})(PostEdit);

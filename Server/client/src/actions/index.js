import axios from 'axios';
import {
  FETCH_USER,
  FETCH_USER_POSTS,
  FETCH_TOP_POSTS,
  FETCH_POST,
  DELETE_POST
} from './types';

export const fetchUser = () => async dispatch => {
  const res = await axios.get('/api/current_user');

  dispatch({ type: FETCH_USER, payload: res.data });
};

export const submitPost = (values, history) => async dispatch => {
  const res = await axios.post('/api/userposts', values);
  history.push('/userposts');
  dispatch({ type: FETCH_POST, payload: res.data });
};

export const fetchUserPosts = () => async dispatch => {
  const res = await axios.get('/api/userposts');

  dispatch({ type: FETCH_USER_POSTS, payload: res.data });
};

export const fetchTopPosts = () => async dispatch => {
  const res = await axios.get('/api/posts/top');
  dispatch({ type: FETCH_TOP_POSTS, payload: res.data });
};

export const fetchPost = id => async dispatch => {
  const res = await axios.get(`/api/userposts/posts/${id}`);
  dispatch({ type: FETCH_POST, payload: res.data });
};

export const deletePost = (id, history) => async dispatch => {
  const res = await axios.delete(`/api/userposts/${id}`);
  if (history) history.push('/userposts');
  dispatch({ type: DELETE_POST, payload: res.data });
};

export const upvotePost = id => async dispatch => {
  const res = await axios.patch(`/api/userposts/upvote/${id}`);
  dispatch({ type: FETCH_POST, payload: res.data });
};

export const downvotePost = id => async dispatch => {
  const res = await axios.patch(`/api/userposts/downvote/${id}`);
  dispatch({ type: FETCH_POST, payload: res.data });
};

export const editPost = (id, values, history) => async dispatch => {
  const res = await axios.patch(`/api/userposts/edit/${id}`, values);
  history.push(`/userposts/posts/${id}`);
  dispatch({ type: FETCH_POST, payload: res.data });
};

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

class Header extends Component {
  renderContent() {
    switch (this.props.auth) {
      case null:
        return;
      case false:
        return (
          <li className="right">
            <a href="/auth/google">Login With Google</a>
          </li>
        );
      default:
        return [
          <li key="1" className="right">
            <a href="/api/logout">Logout</a>
          </li>,
          <li key="2" style={{ margin: '0 10px' }} className="right">
            Username: {this.props.auth.username}
          </li>,
          <li key="3" className={` right ${this.props.history.location.pathname === "/userposts" ? 'highlight' : ''}`}>
            <Link
              to={'/userposts'}
            >
              My posts
            </Link>
          </li>,
        ];
    }
  }

  componentDidMount() {
    this.unlisten = this.props.history.listen(location => {
      this.setState({ location });
    });
  }

  componentWillUnmount() {
    this.unlisten();
  }

  render() {
    return (
      <nav>
        <div className="nav-wrapper teal lighten-2">
          <Link
            to={this.props.auth ? '/userposts' : '/'}
            className="left brand-logo hide-on-small-only"
          >
            PostLite
          </Link>
          <ul className="right ">
            {this.renderContent()}
            <li key="4" className={`hide-on-small-only ${this.props.history.location.pathname === '/top-posts' ? 'highlight' : ''}`}>
              <Link style={{ 'padding': '0 40px', textAlign: 'center' }}
                to={'/top-posts'}
                className="right"
              >
                Top
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default connect(mapStateToProps)(withRouter(Header));

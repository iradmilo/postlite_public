import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import TopPostList from './TopPostList';


class TopPosts extends Component {
  renderContent() {
    if (this.props.auth) {
      return (
        <div className="fixed-action-btn">
          <Link to="/userposts/new" className="btn-floating btn-large red lighten-1 waves-effect waves-light red">
            <i className="material-icons">add</i>
          </Link>
        </div>
      );
    }
  }

  render() {
    return (
      <div style={{ margin: '10px 10px' }}>
        <TopPostList />
        {this.renderContent()}
      </div>
    );
  }
}

function mapStateToProps({ auth }) {
  return { auth };
}

export default connect(mapStateToProps)(TopPosts);
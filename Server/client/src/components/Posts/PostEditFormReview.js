import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import formFields from './formFields';
import { withRouter } from 'react-router-dom';
import * as actions from '../../actions';

const PostEditFormReview = ({ onCancel, formValues, editPost, history, id }) => {

    const reviewFields = _.map(formFields, ({ name, label }) => {
        return (
            <div key={name}>
                <label>{label}</label>
                <div>{formValues[name]}</div>
            </div>
        );
    });

    return (
        <div>
            <h5>Please confirm your entries</h5>
            {reviewFields}
            <button
                className="yellow darken-3 white-text btn-flat"
                onClick={onCancel}
            >
                Back
      </button>
            <button
                onClick={() => editPost(id, formValues, history)}
                className="green btn-flat right white-text"
            >
                Send Post
        <i className="material-icons right">email</i>
            </button>
        </div>
    );
};

function mapStateToProps(state) {
    return { formValues: state.form.postEditForm.values };
}

export default connect(mapStateToProps, actions)(withRouter(PostEditFormReview));

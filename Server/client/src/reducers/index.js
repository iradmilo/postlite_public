import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';
import authReducer from './authReducer';
import topPostsReducer from './topPostsReducer';
import userPostsReducer from './userPostsReducer';

export default combineReducers({
  auth: authReducer,
  form: reduxForm,
  userPosts: userPostsReducer,
  topPosts: topPostsReducer
});
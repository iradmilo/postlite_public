import _ from 'lodash';
import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { Link } from 'react-router-dom';
import PostField from './PostField';
import formFields from './formFields';
import { connect } from 'react-redux';
import { fetchPost } from '../../actions';

class PostEditForm extends Component {

    componentDidMount() {
        this.props.fetchPost(this.props.id);
    }

    renderFields() {
        return _.map(formFields, ({ label, name }) => {
            return (
                <Field
                    key={name}
                    component={PostField}
                    type="text"
                    label={label}
                    name={name}
                />
            );
        });
    }

    render() {
        if (!this.props.initialValues) {
            return <div> Waiting for edit screen </div>
        }
        return (
            <div>
                <h3>Edit post</h3>
                <form onSubmit={this.props.handleSubmit(this.props.onPostEdit)}>
                    {this.renderFields()}
                    <Link
                        to={`/userposts/posts/${this.props.initialValues._id}`}

                        className="red btn-flat white-text btn btn-danger"
                    >
                        Cancel
          </Link>
                    <button type="submit" className="teal btn-flat right white-text">
                        Submit Edited Post
            <i className="material-icons right">done</i>
                    </button>
                </form>
            </div>
        );
    }
}

function validate(values) {
    const errors = {};

    //  if(values.length > 500){
    //      errors["title"] = "Title must be less than 500 charachters long";
    //   }
    _.each(formFields, ({ label, name, length }) => {
        if (values[name] && values[name].length > length) {
            errors[name] = `${label} should be less than ${length} characters long`;
        }
    });

    _.each(formFields, ({ name }) => {
        if (!values[name] && name !== 'link') {
            errors[name] = `You should provide ${name}`;
        }
    });

    return errors;
}


function mapStateToProps(state, ownProps) {
    if (state.userPosts) return { initialValues: state.userPosts[ownProps.id] };
}

export default connect(
    mapStateToProps, { fetchPost }
)(reduxForm({
    validate,
    enableReinitialize: true,
    form: 'postEditForm',
    destroyOnUnmount: false
})(PostEditForm));

